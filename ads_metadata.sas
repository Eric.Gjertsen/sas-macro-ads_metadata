/*Read in Specification document for metadata*/
%global keepvars;

%macro ads_metadata(metadata=, sheet=);

options nosource nonotes nosource2;

proc import out=metadata 
            datafile= "&metadata." 
            dbms=xlsx REPLACE;
            sheet="&sheet.";             
run;

%if %sysfunc(exist(metadata)) %then %do;

data attrib;
    set metadata;
	where upcase(keepvar)="YES";
    if lowcase(type)in ("text") then do;
    varlength= "$"|| strip(put(length, best.));
  
    labels=variable || ' label=  "' || strip(label) || '" length=' ||varlength;
      end;
      else labels= variable || ' label=  "' || strip(label) || '"' ;
run;

proc sql noprint;
select trim(labels) into :attrib separated by " "
from attrib
where variable ne " ";

select upcase(variable) into :keepvars separated by " "
from attrib
where variable ne " ";
quit;

options missing = ' ';
/*Use this dataset to stack with final data*/
data &sheet._shell;
        attrib &attrib.;
     call missing (of _all_);
	 if missing(cats(of _all_)) then delete;
run;

%end;
%else %do;
      %put Import of analysis specificaiton was not succesful. Please provide the correct name of the specification;
	  %put Please check the path of the study folder;
%end;

options source notes source2;

%mend ads_metadata;


